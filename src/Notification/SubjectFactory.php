<?php
declare(strict_types=1);

namespace App\Notification;


final class SubjectFactory implements SubjectFactoryInterface
{
    public function buildFrom($from): string
    {
        return 'subject';
    }
}
