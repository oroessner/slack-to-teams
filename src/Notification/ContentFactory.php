<?php
declare(strict_types=1);

namespace App\Notification;


final class ContentFactory implements ContentFactoryInterface
{

    public function buildFrom($from): string
    {
        return 'content';
    }
}
