<?php
declare(strict_types=1);

namespace App\Notification;

interface SubjectFactoryInterface extends StringFactoryInterface
{
}
