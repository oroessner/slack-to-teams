<?php
declare(strict_types=1);

namespace App\Notification;

interface StringFactoryInterface
{
    public function buildFrom($from):string;
}
