<?php
declare(strict_types=1);

namespace App\Action;

use App\Notification\ContentFactoryInterface;
use App\Notification\StringFactoryInterface;
use App\Notification\SubjectFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Notifier\Exception\LogicException;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Routing\Annotation\Route;

final class SendAction
{
    public function __construct(
        private NotifierInterface $notifier,
        private SubjectFactoryInterface $subjectFactory,
        private ContentFactoryInterface $contentFactory,
    )
    {
    }

    #[Route('/{from}/{to}', name: 'send', methods: ['GET', 'HEAD', 'POST'])]
    public function __invoke(string $from, string $to): Response
    {
        $subject = $this->subjectFactory->buildFrom($from);
        $content = $this->contentFactory->buildFrom($from);

        $notification = (new Notification($subject, [$to]))
            ->content($content);

        try {
            $this->notifier->send($notification);
        } catch (LogicException $exception) {
            throw new BadRequestHttpException($exception->getMessage(), $exception);
        }

        return new JsonResponse(['from' => $from, 'to' => $to]);
    }
}
